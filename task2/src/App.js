import React, { Component } from 'react';

import './App.css';

class App extends Component {

    state = {
        jokes: '',


    };


    constructor(props) {
        super(props);

    }
    getJokes=() => {
        const P_URL = 'https://api.chucknorris.io/jokes/random';
        fetch(P_URL).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');
        }).then(posts => {
            // const updatedJokes = posts.map(post => {
            //     return {
            //         ...post,
            //         author: 'John Doe'
            //     }
            // });
            console.log(posts)

            this.setState({jokes: posts.value});
        }).catch(error => {
            console.log(error);
        });
    };
    componentDidMount() {
        this.getJokes();
    }

    render() {
        return (
            <div className="container text-center">
                <h1>Chuck Norris Facts</h1>
                <h3>{'"' + this.state.jokes + '"'}</h3>
                <button type="button"
                        className="btn btn-primary"
                       onClick={this.getJokes} >
                    More Chuck Jokes!
                </button>
            </div>
    );
  }
}

export default App;
