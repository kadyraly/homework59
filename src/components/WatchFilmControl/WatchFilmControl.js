import React from 'react';
import  './WatchFilmControl.css';

const WatchFilmControl =(props) => {
    return (
        <div>

            <input className="inputText" type="text" placeholder="Film name" value={props.text} onChange={props.textChange} />


            <button onClick ={props.click} className="WatchFilmControl-btn">
                Add
            </button>
        </div>
    );

};
export default  WatchFilmControl