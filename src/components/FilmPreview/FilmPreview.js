import React, {Component} from 'react';
import './FilmPreview.css';
import Movie from "../Movie/Movie";

const FilmPreview =(props) => {
    return (
        <div className="FilmPreview">
            <p>To watch list:</p>
            {props.films.map((film, i) => {
                return <Movie change={(e) => props.change(e, film.id)} value={film.text} key={film.id} remove={() => props.remove(film.id)}/>
                // return <div><input key={film.id} value={film.text} onChange={(e) => props.change(e, film.id)} /><span onClick={() => props.remove(film.id)}>X</span></div>
            })}

        </div>
    )
};

export default FilmPreview;