import React, {Component} from 'react';

import './Cinema.css';
import WatchFilmControl from "../components/WatchFilmControl/WatchFilmControl";
import FilmPreview from "../components/FilmPreview/FilmPreview";



class Cinema extends Component {
    state = {
        films: [],
        text: '',

    };



    handleFilmChange = (e) => {
        this.setState({text: e.target.value});
    };


    handleChange=(e, id) => {
        const films = [...this.state.films];
        const index = films.findIndex((film) => film.id === id);
        const film = {...this.state.films[index]};
        film.text = e.target.value;
        films[index] = film;
        this.setState({films: films})
    };





    addFilms = (e) => {
        e.preventDefault();
        const films = [...this.state.films];
        films.push({
            text: this.state.text,

            id: Date.now()
        });
        this.setState({films});
    };
    removeFilms = (id) => {
        this.setState({films: this.state.films.filter((film, index) => film.id !==id)})

    };




    render() {
        return (
            <div className="Cinema">

                <WatchFilmControl
                    text={this.state.text}
                    textChange={this.handleFilmChange}
                    click={this.addFilms}
                />
                <FilmPreview films={this.state.films} change={this.handleChange} remove={this.removeFilms}/>
            </div>
        );
    }
}

export default Cinema;